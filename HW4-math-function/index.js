// Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера два числа.
// Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
// Вивести у консоль результат виконання функції.

let numberFirst = +prompt('Input first number');
let numberSecond = +prompt('Input second number');
let mathOperator = prompt('Input math operator');

function getResult(number1, number2, operator) {
  switch (operator) {
    case '+':
      return number1 + number2;
    case '-':
      return number1 - number2;
    case '/':
      return number1 / number2;
    case '*':
      return number1 * number2;
    default:
      return 0;
  }
}

console.log(getResult(numberFirst, numberSecond, mathOperator));
