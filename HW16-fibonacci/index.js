// Реализовать функцию для подсчета n-го обобщенного числа Фибоначчи.

// Технические требования:
// Написать функцию для подсчета n-го обобщенного числа Фибоначчи. Аргументами на вход будут три числа - F0, F1, n, где F0, F1 - первые два числа последовательности (могут быть любыми целыми числами), n - порядковый номер числа Фибоначчи, которое надо найти. Последовательнось будет строиться по следующему правилу F2 = F0 + F1, F3 = F1 + F2 и так далее.
// Считать с помощью модального окна браузера число, которое введет пользователь (n).
// С помощью функции посчитать n-е число в обобщенной последовательности Фибоначчи и вывести его на экран.
// Пользователь может ввести отрицательное число - результат надо посчитать по такому же правилу (F-1 = F-3 + F-2).

let number = +prompt('Input number');
while (!Number.isFinite(number)) {
  number = +prompt('Input number');
}

function fib(num) {
  let prev = 1;
  let curr = 1;
  for (let i = 2; i < num; i++) {
    const result = prev + curr;
    prev = curr;
    curr = result;
  }
  return num === 0 || num === 1 ? num : curr;
}

console.log(fib(number));

//----------------------------------------------------
// Варіант з рекурсією

// function fib(n) {
//   if (n < 0) return fib(n * -1) * -1;
//   if (n === 0 || n === 1) {
//     return n;
//   } else {
//     return fib(n - 1) + fib(n - 2);
//   }
// }

// console.log(fib(number));
