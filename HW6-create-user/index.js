// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем.

// Технічні вимоги:
// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// Створити метод getAge() який повертатиме скільки користувачеві років.
// Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.

function createNewUser() {
  let firstName = prompt('Input your first Name');
  let lastName = prompt('Input your last Name');
  let birthday = prompt('Input your birthday "dd.mm.yyyy"');

  let newbirthday = birthday.split('.').reverse().join(',');

  let birthDate = new Date(newbirthday);
  let birthDateDay = birthDate.getDate();
  let birthDateMonth = birthDate.getMonth();
  let birthDateYear = birthDate.getFullYear();

  let todayDate = new Date();
  let todayDay = todayDate.getDate();
  let todayMonth = todayDate.getMonth();
  let todayYear = todayDate.getFullYear();

  const newUser = {
    firstName,
    lastName,
    birthday,
    getAge() {
      if (todayMonth > birthDateMonth) {
        return `Ваш вік: ${todayYear - birthDateYear}`;
      } else if (todayMonth === birthDateMonth) {
        if (todayDay >= birthDateDay) {
          return `Ваш вік: ${todayYear - birthDateYear}`;
        } else return `Ваш вік: ${todayYear - birthDateYear - 1}`;
      } else return `Ваш вік: ${todayYear - birthDateYear - 1}`;
    },
    getPassword() {
      return (
        this.firstName.slice(0, 1).toUpperCase() +
        this.lastName.toLowerCase() +
        birthDateYear
      );
    },

    getLogin() {
      return (this.firstName.slice(0, 1) + this.lastName).toLowerCase();
    },
  };

  console.log(newUser.getAge());
  console.log(newUser.getPassword());
  console.log(newUser.getLogin());

  Object.defineProperty(newUser, 'firstName', {
    setFirstName(newName) {
      this.firstName = newName;
    },
    writable: false,
  });

  Object.defineProperty(newUser, 'lastName', {
    setLastName(newLastName) {
      this.lastName = newLastName;
    },
    writable: false,
  });
}
createNewUser();
