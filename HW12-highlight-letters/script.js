// Реалізувати функцію підсвічування клавіш.

// Технічні вимоги:
// У файлі index.html лежить розмітка для кнопок.
// Кожна кнопка містить назву клавіші на клавіатурі
// Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.

document.body.addEventListener('keyup', (e) => {
  document.querySelectorAll('.btn').forEach((el) => {
    el.classList.remove('btn-active');
    if (
      el.innerHTML === e.key.toUpperCase() ||
      el.innerHTML === e.key.slice(0, 1) + e.key.slice(1).toLowerCase()
    ) {
      el.classList.add('btn-active');
      console.log(el.innerHTML);
    }
  });
});
