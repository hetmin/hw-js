// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку.

// Технічні вимоги:
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

// ---------------------------------------------------------


function returnList(arr, tag) {
  if (tag) {
    const createTag = document.createElement(tag);
    document.body.append(createTag);
    arr.forEach((element) => {
      createTag.insertAdjacentHTML('beforeend', '<li>' + element + '</li>');
    });
  } else {
    arr.forEach((element) => {
      document.body.insertAdjacentHTML('beforeend', '<li>' + element + '</li>');
    });
  }
}

returnList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'], 'ul');

//-----Timer----

let sec = 5;
function timeSec() {
  if (sec < 1) {
    document.body.innerHTML = '';
  } else {
    document.getElementById('timer').innerHTML = sec;
    setTimeout(timeSec, 1000);
    sec--;
  }
}
timeSec();
