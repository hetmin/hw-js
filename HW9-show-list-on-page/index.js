// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку.

// Технічні вимоги:
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

function returnList(arr, tag = document.body) {
  const arrList =
    '<ul>' +
    arr.map((el) => {
      return '<li>' + el + '</li>';
    }) +
    '</ul>';

  const newList = arrList.split(',').join('');

  if (tag !== document.body) {
    const createTag = document.createElement(tag);
    document.body.append(createTag);
    createTag.insertAdjacentHTML('beforeend', newList);
  } else {
    document.body.insertAdjacentHTML('beforeend', newList);
  }
}

returnList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'], 'div');

//-----Timer----

let sec = 12;
function timeSec() {
  if (sec < 1) {
    document.body.innerHTML = '';
  } else {
    document.getElementById('timer').innerHTML = sec;
    setTimeout(timeSec, 1000);
    sec--;
  }
}
timeSec();
