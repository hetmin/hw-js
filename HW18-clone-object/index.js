// Реализовать функцию полного клонирования объекта. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
// Технические требования:
// Написать функцию для рекурсивного полного клонирования объекта (без единой передачи по ссылке, внутренняя вложенность свойств объекта может быть достаточно большой).
// Функция должна успешно копировать свойства в виде объектов и массивов на любом уровне вложенности.
// В коде нельзя использовать встроенные механизмы клонирования, такие как функция Object.assign() или спред-оператор.

const book = {
  title: 'myTitle',
  author: 'myAthor',
  pages: 256,
  orice: 15,
  size: { heigth: 15, width: 89 },
};

// Варіант 1
function copyObject(newObj, obj) {
  for (let key in obj) {
    if (typeof obj[key] === 'object') {
      newObj[key] = copyObject({}, obj[key]);
    } else newObj[key] = obj[key];
  }
  return newObj;
}

let otherBook = copyObject({}, book);
console.log(otherBook);

// Варіант 2
let otherBook2 = JSON.parse(JSON.stringify(book));
console.log(otherBook2);
