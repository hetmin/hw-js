1. Опишіть своїми словами що таке Document Object Model (DOM)
   Об'єктна модель документа.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
   innerText - повертає тільки текстовий вміст елементів
   innerHTML - повертає HTML теги і текст

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
   querySelector
   querySelectorAll
   getElementById
   getElementsByName
   getElementsByTagName
   getElementsByClassName

Кращим є:
querySelector
querySelectorAll
