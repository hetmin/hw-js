// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const paragraphs = document.querySelectorAll('p');
for (let par of paragraphs) {
  par.style.backgroundColor = '#ff0000';
  console.log(par.innerHTML);
}

console.log('-----------------------------------');

// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

console.log(
  '--------------Знайти елемент із id="optionsList"--------------------'
);
options = document.getElementById('optionsList');
console.log(options.innerHTML);

console.log('-------------Знайти батьківський елемент---------------------');
let parentEl = options.closest('.container');
console.log(parentEl.innerText);

console.log('-------------Знайти дочірні ноди----------------------');
let childNodesEl = options.childNodes;
for (let childsEl of childNodesEl) {
  console.log(childsEl.nodeName);
}

// Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// This is a paragraph

let testParag = document.querySelector('#testParagraph');
testParag.innerHTML = 'This is a paragraph';

console.log('-----------------------------------------------');

// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

console.log('-Отримати елементи, вкладені в елемент із класом main-header-');

let mainHeader = document.querySelectorAll('.main-header .main-nav-item');

mainHeader.forEach((el) => {
  el.classList.add('nav-item');
  console.log(el);
});

console.log('------Знайти всі елементи із класом section-title-------');

// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
let findElments = document.querySelectorAll('.section-title');
for (let el of findElments) {
  el.classList.remove('section-title');
}
