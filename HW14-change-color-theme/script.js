// Реалізувати можливість зміни колірної теми користувача.

// Технічні вимоги:
// Взяти будь-яке готове домашнє завдання з HTML/CSS.
// Додати на макеті кнопку "Змінити тему".
// При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
// Вибрана тема повинна зберігатися після перезавантаження сторінки

document.querySelector('.btn').addEventListener('click', () => {
  if (localStorage.getItem('theme') === 'new') {
    localStorage.removeItem('theme');
    location.reload();
  } else {
    if (localStorage.getItem('theme') !== 'new') {
      localStorage.setItem('theme', 'new');
      location.reload();
    }
  }
});

if (localStorage.getItem('theme') === 'new') {
  document
    .querySelector('.registration-log')
    .classList.add('registration-log-new');

  document
    .querySelector('.registration-sign')
    .classList.add('registration-sign-new');

  document.querySelector('.btn-google').classList.add('btn-google-new');

  document.querySelector('body').classList.add('body-new');

  document.querySelector('.btn-facebook').classList.add('btn-facebook-new');
}
