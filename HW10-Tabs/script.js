const tabsTitle = document.querySelectorAll('.tabs-title');
const tabsContent = document.querySelectorAll('.content');
let tabName;

tabsTitle.forEach((el) => {
  el.addEventListener('click', selectTabsTitle);
});

function selectTabsTitle() {
  tabsTitle.forEach((el) => {
    el.classList.remove('active');
    this.classList.add('active');
    tabName = this.getAttribute('data-avtive');
  });
  tabsContent.forEach((el) => {
    el.classList.remove('active');
    if (el.classList.contains(tabName)) {
      el.classList.add('active');
    }
  });
}
