// Створити об'єкт "студент" та проаналізувати його табель.

// Технічні вимоги:
// Створити порожній об'єкт student, з полями name та lastName.
// Запитати у користувача ім'я та прізвище студента, отримані значення записати у відповідні поля об'єкта.
// У циклі запитувати у користувача назву предмета та оцінку щодо нього. Якщо користувач натисне Cancel при n-питанні про назву предмета, закінчити цикл. Записати оцінки з усіх предметів як студента tabel.
// порахувати кількість поганих (менше 4) оцінок з предметів. Якщо таких немає, вивести повідомлення "Студент переведено на наступний курс".
// Порахувати середній бал з предметів. Якщо він більше 7 – вивести повідомлення Студенту призначено стипендію.

const student = {
  firstName: null,
  lastName: null,
};

const firstName = prompt('Input first Name');
const lastName = prompt('Input last Name');

student.firstName = firstName;
student.lastName = lastName;

console.log(student);

const tabel = {};
while (true) {
  const subjectName = prompt('Input subject Name');
  const rating = +prompt('Input rating');
  if (!subjectName || !rating) {
    break;
  }

  tabel[subjectName] = rating;
}
console.log(tabel);

let sumBadRaiting = 0;
let counter = 0;
let sumRaiting = 0;
for (let key in tabel) {
  if (tabel[key] < 4) {
    sumBadRaiting++;
  }
  counter++;
  sumRaiting += tabel[key];
}

if (sumBadRaiting === 0) {
  console.log('Студент переведено на наступний курс');
}

console.log('Cередній бал з предметів = ' + sumRaiting / counter);
if (sumRaiting / counter > 7) {
  console.log('Студенту призначено стипендію');
}
console.log(
  'Rількість поганих (менше 4) оцінок з предметів = ' + sumBadRaiting
);
