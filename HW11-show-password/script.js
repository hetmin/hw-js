// Написати реалізацію кнопки "Показати пароль".

// Технічні вимоги:
// У файлі index.html лежить розмітка двох полів вводу пароля.
// Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
// Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
// Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
// Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
// Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
// Після натискання на кнопку сторінка не повинна перезавантажуватись
// Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.

const form = document.querySelector('form');

form.addEventListener('click', (e) => {
  if (e.target.getAttribute('type') === 'password') {
    e.target.setAttribute('type', 'text');
    if (e.target.classList.contains('fa-eye-slash')) {
      e.target.classList.toggle('non-activ');
    }
  } else {
    e.target.setAttribute('type', 'password');

    if (e.target.classList.contains('fa-eye-slash')) {
      e.target.classList.toggle('non-activ');
    }
  }
});

// Submit
const inputOne = document.querySelector('.input-one');
const inputTwo = document.querySelector('.input-two');
const passwordError = document.querySelector('.error-password');

form.addEventListener('submit', (e) => {
  e.preventDefault();
  if (inputOne.value && inputTwo.value && inputOne.value === inputTwo.value) {
    passwordError.classList.remove('active-error');
    alert('You are welcome');
  } else {
    passwordError.classList.add('active-error');
  }
});
