let number;
while (!Number.isInteger(number)) {
  number = +prompt('Input integer number');
}

let count = 0;
for (let i = 0; i <= number; i++) {
  if (i % 5 === 0) {
    console.log(i);
    count++;
  }
}

if (count === 1) {
  console.log('Sorry, no numbers');
}
